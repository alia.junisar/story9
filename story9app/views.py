from django.shortcuts import render
from django.shortcuts import redirect
from django.http import HttpResponse
from datetime import datetime
from django.contrib.auth import authenticate, login as auth_login

# Create your views here.

def index(request):
	return render(request, 'story9app/index.html')

def login(request):
	if request.user.is_authenticated:
		return redirect('story9app:index')

	else:
		if request.method == 'POST':
			username = request.POST.get('username')
			password = request.POST.get('password')
			user = authenticate(request, username=username, password=password)

			if user is not None:
				auth_login(request.user)
				request.session['login timestamp'] = str(datetime.now())
				return redirect('story9app:index')

			else:
				pass
				return render(request, 'story9app/login.html', 'error':'Username or password does not match')


	return render(request, 'story9app/login.html')

def logout(request):
	auth_logout(request)
	return redirect('story9app:index')