from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.utils import timezone
from django.contrib.auth import login
from djagno.contrib.auth.models import User
from .views import index
from .apps import Story9AppConfig

class Story9TestCase(TestCase):

    # def test_story_9_url_is_exist(self):
    # 	response = Client().get('/')
    # 	self.assertEqual(response.status_code, 200)
    
    # def test_story_9_using_status_func(self):
    # 	found = resolve('/')
    # 	self.assertEqual(found.func, index)
    
    # def test_story_9_using_status_template(self):
    #     response = Client().get('/')
    #     self.assertTemplateUsed(response, 'index.html')
    @classmethod
    def setUpClass(self):
    	self.user = User.objects.create_user('tester', 'test123@gmail.com', 'test123')

    def test_hompage_is_exist(self):
    	response = self.client.get(reverse('story9app:index'))
    	self.assertEqual(response.status_code, 200)

    def test_is_not_authenticated(self):
    	response = self.client.get(reverse('story9app:index'))
    	html = response.content.decode()
    	self.assertIn('Sorry, you are not logged in', html)
    	self.assertNotIn(self.user.username, html)